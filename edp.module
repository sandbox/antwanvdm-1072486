<?php
/**
 * Modules that handles basic implementations of the package (settings reset, help, permission)
 * 
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */

/**
 * Implements hook_help().
 */
function edp_help( $path, $arg ) {
  switch ( $path ) {
    case "admin/help#edp":
      //Load information out of HELP_HTML.txt file (same text as on project on drupal.org)
      //Also, same text as README.txt but HTML formatted
      $help_text = file_get_contents( drupal_get_path( "module", "edp" ) . "/HELP_HTML.txt" );
      return $help_text;
  }
}

/**
 * Permission Hook to give the application access to this module
 * 
 * @return array
 */
function edp_permission() {
  return array(
    'edp' => array(
      'title' => t( 'Access Extended Drupal Package content' ), 
      'description' => t( 'Use the edp (sub)module(s) within the code' ) 
    ) 
  );
}

/**
 * Implementation of hook_flush_caches()
 * While flushing caches, make sure variables get reset
 */
function edp_flush_caches() {
  _edp_reset_settings_variables();
  return array();
}

/**
 * Get all the available settings by domain within a key => value array
 */
function edp_get_settings_by_current_domain() {
  //Select first table
  $query = db_select( "node", 'value' );
  
  //Joins for more tables to get all data
  $query->addJoin( "LEFT", "field_data_field_domain", "dorel", "value.nid = dorel.entity_id" );
  $query->addJoin( "LEFT", "node", "domain", "dorel.field_domain_nid = domain.nid" );
  $query->addJoin( "LEFT", "field_data_field_key", "keyrel", "value.nid = keyrel.entity_id" );
  $query->addJoin( "LEFT", "taxonomy_term_data", "skey", "keyrel.field_key_tid = skey.tid" );
  
  //Get needed fields from tables
  $query->fields( 'skey', array(
    'name' 
  ) );
  $query->fields( 'value', array(
    'title' 
  ) );
  
  //Conditions for correct type & domain
  $query->condition( "value.type", 'edp_setting', "=" );
  $query->condition( "domain.title", $_SERVER ['HTTP_HOST'], "=" );
  
  //Execute into keyed Array
  $settings = $query->execute()->fetchAllKeyed();
  
  return $settings;
}

/**
 * Resets all variables within the Drupal variable table.
 * All changes within edp_settings will be active within variable table
 */
function _edp_reset_settings_variables() {
  $settings = edp_get_settings_by_current_domain();
  
  //Loop through settings to delete old vars, and set the brand new settings!
  //If current domain is not present in Config, this loop will NOT be called
  foreach ( $settings as $key => $value ) {
    variable_del( "edp_" . strtolower( $key ) );
    variable_set( "edp_" . strtolower( $key ), $value );
  }
}