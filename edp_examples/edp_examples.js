/**
 * Document.ready for this .JS with startup functions
 */
jQuery(document).ready(function() {
  // Click handlers for Social Media login buttons
  jQuery(".fb-click").click(loginPopupFacebook);
  jQuery(".tw-click").click(loginPopupTwitter);
  jQuery(".hy-click").click(loginPopupHyves);
});

/**
 * Return function from socialMedia javascripts Use this functionname to receive the data
 * 
 * @param data object
 */
function handleSocialMediaData(data) {
  switch ( data.source ) {
    case "facebook" :
      console.log("Facebook");
      console.log(data);
      break;

    case "hyves" :
      console.log("Hyves");
      console.log(data);
      break;

    case "twitter" :
      console.log("Twitter");
      console.log(data);
      break;
  }
}