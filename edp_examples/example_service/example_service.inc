<?php
/**
 * @file
 * Module that interacts with Services Module [for AMF/JSON/Etc]
 * 
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */

/**
 * Get the the current user
 * 
 * @return object
 */
function example_service_get_user() {
  return edp_socialmedia_interface()->getUser();
}

/**
 * Get all friends for the logged-in user
 * 
 * @return object
 */
function example_service_get_friends() {
  return edp_socialmedia_interface()->getFriends();
}

/**
 * Post a status update
 * 
 * @param array $data
 * @return object
 */
function example_service_post_status_update($data) {
  return edp_socialmedia_interface()->postStatusUpdate( $data );
}