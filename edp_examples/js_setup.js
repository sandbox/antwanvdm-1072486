/**
 * File that includes some basic Javascript to be used later on in the application
 */

// Make sure, console.log() doesn't break in IE
if (!console) {
  var console = {
    log : function() {
    }
  };
}

/**
 * Test log settings
 */
function showSettingsLog() {
  console.log(Drupal.settings.edpsettings.data.SITE_GA_CODE);
};