<?php
/**
 * Modules that sets up a custom API to get data out of menu callbacks
 * 
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */

/**
 * Implements hook_menu().
 */
function edp_customapi_menu() {
  $items ['ajax/node/%'] = array(
    'page callback' => '_edp_customapi_ajax_get_node', 
    'page arguments' => array(
      2 
    ), 
    'access arguments' => array(
      'edp' 
    ), 
    'type' => MENU_CALLBACK 
  );
  $items ['ajax/gacode'] = array(
    'page callback' => '_edp_customapi_ajax_get_ga_code', 
    'access arguments' => array(
      'edp' 
    ), 
    'type' => MENU_CALLBACK 
  );
  
  return $items;
}

/**
 * Get a node by an AJAX call and return as JSON
 * 
 * @uses     Drupal Hook to modify the node to custom format
 * @example  function mymodule_customapi_ajax_get_node($node) {
 *             $class = new stdClass();
 *             $class->id = $node->nid;
 *             $class->title = $node->title;
 *         
 *             return $class;
 *           }
 * @param int $nid
 */
function _edp_customapi_ajax_get_node($nid) {
  $node = node_load( $nid );
  
  //Make hook available
  $class = module_invoke_all( "customapi_ajax_get_node", $node );
  if (is_array( $class ) && ! empty( $class )) {
    $node = $class;
  }
  
  drupal_json_output( $node );
}

/**
 * Get your Google Analytics code by an AJAX call
 */
function _edp_customapi_ajax_get_ga_code() {
  drupal_json_output( variable_get( "edp_site_ga_code" ) );
}