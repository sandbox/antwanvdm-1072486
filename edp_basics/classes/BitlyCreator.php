<?php
/**
 * Class to create a Bit.ly link
 *
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */
class BitlyCreator {
  /**
   * Login name from bit.ly
   * @var string
   */
  private $login;
  
  /**
   * API Key from bit.ly
   * @var string
   */
  private $appkey;
  
  /**
   * Initialise BitlyCreator object
   *
   * @param   String   $login    Login name from Bit.ly account
   * @param   String   $appkey   Bit.ly API key
   * @return  void
   */
  public function __construct($login, $appkey) {
    $this->login = $login;
    $this->appkey = $appkey;
  }
  
  /**
   * Shorten an URL
   *
   * @param   String   $url URL to be shortened
   * @return  String        URL that has been shortened
   */
  public function shortenUrl($url) {
    return $this->curlGetResult( "http://api.bit.ly/v3/shorten?login={$this->login}&apiKey={$this->appkey}&uri=" . urlencode( $url ) . "&format=txt" );
  }
  
  /**
   * Expand an URL that was previously shortened
   *
   * @param   String   $url URL to be expanded
   * @return  String        URL that has been expanded
   */
  public function expandUrl($url) {
    return $this->curlGetResult( "http://api.bit.ly/v3/expand?login={$this->login}&apiKey={$this->appkey}&shortUrl=" . urlencode( $url ) . "&format=txt" );
  }
  
  /**
   * Make the call to bit.ly and get the result
   * 
   * @param String $url
   */
  private function curlGetResult($url) {
    $ch = curl_init();
    $timeout = 5;
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
    $data = curl_exec( $ch );
    curl_close( $ch );
    return $data;
  }
}
?>