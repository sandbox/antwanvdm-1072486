<?php
/**
 * Class with common Site Utilities to use within application
 *
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 * 
 * @copyright This is an edited version of the original Class found at:
 * @link http://code.google.com/p/php-mobile-detect/issues/attachmentText?id=4&aid=5276058426341450480&name=Mobile_Detect.php&token=30de7140b90513f56c437e222a98d0a4
 */
class Mobile_Detect {
  /**
   * Uses $_SERVER ['HTTP_ACCEPT']
   * @var string
   */
  protected $accept;
  
  /**
   * Uses $_SERVER ['HTTP_USER_AGENT']
   * @var string
   */
  protected $userAgent;
  
  /**
   * Defines if user is on a mobile device
   * @var bool
   */
  protected $isMobile = FALSE;
  
  /**
   * Defines if user is on a Apple device
   * @var NULL
   */
  protected $isApple = NULL;
  
  /**
   * Defines if user is on a Android device
   * @var NULL
   */
  protected $isAndroid = NULL;
  
  /**
   * Defines if user is on a Blackberry device
   * @var NULL
   */
  protected $isBlackberry = NULL;
  
  /**
   * Defines if user is on a Opera device
   * @var NULL
   */
  protected $isOpera = NULL;
  
  /**
   * Defines if user is on a Palm device
   * @var NULL
   */
  protected $isPalm = NULL;
  
  /**
   * Defines if user is on a Windows device
   * @var NULL
   */
  protected $isWindows = NULL;
  
  /**
   * Defines if user is on any other mobile device
   * @var NULL
   */
  protected $isGeneric = NULL;
  
  /**
   * Array of all possible user agents within the different mobile categories
   * @var array
   */
  protected $devices = array(
    "android" => "android", 
    "blackberry" => "blackberry", 
    "apple" => "(iphone|ipod|ipad)", 
    "opera" => "opera mini", 
    "palm" => "(avantgo|blazer|elaine|hiptop|palm|plucker|xiino)", 
    "windows" => "windows ce; (iemobile|ppc|smartphone)", 
    "generic" => "(kindle|mobile|mmp|midp|o2|pda|pocket|psp|symbian|smartphone|treo|up.browser|up.link|vodafone|wap)" 
  );
  
  /**
   * Init the object and check device status
   */
  public function __construct() {
    $this->userAgent = $_SERVER ['HTTP_USER_AGENT'];
    $this->accept = $_SERVER ['HTTP_ACCEPT'];
    
    if (isset( $_SERVER ['HTTP_X_WAP_PROFILE'] ) || isset( $_SERVER ['HTTP_PROFILE'] )) {
      $this->isMobile = TRUE;
    } elseif (strpos( $this->accept, 'text/vnd.wap.wml' ) > 0 || strpos( $this->accept, 'application/vnd.wap.xhtml+xml' ) > 0) {
      $this->isMobile = TRUE;
    } else {
      foreach ( $this->devices as $device => $regexp ) {
        if ($this->isDevice( $device )) {
          $this->isMobile = TRUE;
        }
      }
    }
  }
  
  /**
   * Overloads isIphone() | isAndroid() | isBlackberry() | isOpera() | isPalm() | isWindows() | isGeneric() through isDevice()
   *
   * @param string $name
   * @param array $arguments
   * @return bool
   */
  public function __call($name, $arguments) {
    $device = substr( $name, 2 );
    if ($name == "is" . ucfirst( $device )) {
      return $this->isDevice( $device );
    } else {
      trigger_error( "Method $name not defined", E_USER_ERROR );
    }
  }
  
  /**
   * Returns TRUE if any type of mobile device detected, including special ones
   * 
   * @return bool
   */
  public function isMobile() {
    return $this->isMobile;
  }
  
  /**
   * Check is the given device is the expected device
   * 
   * @param string $device
   * @return bool
   */
  protected function isDevice($device) {
    $var = "is" . ucfirst( $device );
    $return = $this->$var === NULL ? ( bool ) preg_match( "/" . $this->devices [$device] . "/i", $this->userAgent ) : $this->$var;
    
    if ($device != 'generic' && $return == TRUE) {
      $this->isGeneric = FALSE;
    }
    
    return $return;
  }
}
