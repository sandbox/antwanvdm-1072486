<?php
/**
 * Class with common Site Utilities to use within application
 *
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */
class SiteUtils {
  /**
   * Initialise SiteBasics object
   */
  public function __construct() {
    //
  }
  
  /**
   * Validate a given IP Address
   * @link http://us2.php.net/manual/en/filter.filters.flags.php
   *
   * @param string $ip
   * @param int $flag   FILTER_FLAG_IPV4 - Allow only IPv4 address in "validate_ip" filter.
   * FILTER_FLAG_IPV6 - Allow only IPv6 address in "validate_ip" filter.
   * FILTER_FLAG_NO_PRIV_RANGE - Deny reserved addresses in "validate_ip" filter.
   * FILTER_FLAG_NO_RES_RANGE - Deny private addresses in "validate_ip" filter.
   * @return bool
   */
  public function validIpAddress($ip, $flag = FILTER_FLAG_NONE) {
    if (filter_var( $ip, FILTER_VALIDATE_IP, $flag )) {
      return TRUE;
    } else {
      return FALSE;
    }
  }
  
  /**
   * Prepend zero digitits to current number,
   * example: change 456 into 00456
   * 
   * @param int $int    Used int to prepend with zero's
   * @param int $length Total length of the returned digit
   * @return string
   */
  public function prependZeroDigits($int, $length) {
    $currentLength = strlen( ( string ) $int );
    
    if ($currentLength < $length) {
      for ($i = 0; $i < ($length - $currentLength); $i ++) {
        $int = "0" . $int;
      }
    }
    
    return $int;
  }
  
  /**
   * Pass an array by reference and shuffle contents in randon order
   *
   * @param array $array
   * @return bool
   */
  public function shuffleArray(&$array) {
    if (count( $array ) > 1) { //$keys needs to be an array, no need to shuffle 1 item anyway
      $keys = array_rand( $array, count( $array ) );
      
      foreach ( $keys as $key )
        $new [$key] = $array [$key];
      
      $array = $new;
    }
    return TRUE; //because it's a wannabe shuffle(), which returns TRUE
  }
}
?>