=======
SUMMARY
=======
The Extended Drupal Package is used to add Best Practices to your
Drupal installation. Ready-to-go implementations are provided to be
used as a layer in any kind of Drupal project.

The package is mainly build for developers as a nice and handy 
library to use within their development process. Contact me if you 
have any ideas or suggestions to what might be useful for the package. 
Also don't hesitate if you're interested in becoming a co-maintainer 
for this project.

==============================================
WHAT DOES THE EXTENDED DRUPAL PACKAGE PROVIDE?
==============================================
- Feature:
  Provides content types, taxonomy & views as startup for the package
- Settings:
  Starting point of package to configure all your domains and settings
  (like GA code & Social Media keys)
- Basic Utilities:
  Provides classes like SiteUtils & MobileDetect as library to use 
  within your own code
- Custom API:
  Provides menu URL's to return data as JSON
- Social Media:
  Provides Social Media support, as a start Facebook, Twitter & Hyves.
    - Social Interface:
      Communicate with one interface that handles corresponding 
      functionality from different Social Media
    - Share links:
      Get access to the default share links from different Social Media
- Examples:
  Provides examples on implementations for the package 

=========
LIBRARIES
=========
For some modules, different libraries are used. See here which are used
within the package:

- MobileDetect.php Class
- Facebook API: php sdk
- Twitter API: twitteroauth
- Hyves API: genusapis

=====================
EXTERNAL DEPENDENCIES
=====================
- Taxonomy, Field, Field SQL storage, Options, PHP Filter (within core)
- Features
- Views
- Chaos tools
- References & Node Reference
- Strongarm
- Services (optional)
- Devel (optional)

=====================
INTERNAL DEPENDENCIES
=====================
Within the package everything depends on the main Extended Drupal 
Package module. This module itself depends on the provided edp feature 
that sets up the data structure needed to work with the package.

=====================
CONFIGURATION PROCESS
=====================
1. Upload (or install) the package on the server at sites/all/modules
2. Activate edp | Feature module to initialize the needed content 
   structure
3. Activate modules within the Extended Drupal Package area that
   you wish to use
4. Set Permissions for the package on the
   admin/people/permissions#module-edp page
5. Add domains & settings within admin/config/edp for used modules
   (for example: Facebook keys)
6. After transferring site to other domain, clear cache and new variables
   are ready to go!
7. Check out the Examples module for how to use all the functionality

=======
CREDITS
=======
- Module is sponsored by dpdk
- More information from the process of this package if found at my 
  graduation website (Dutch!) http://drupal.antwan.eu