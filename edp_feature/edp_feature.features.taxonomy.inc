<?php
/**
 * @file
 * edp_feature.features.taxonomy.inc
 */

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function edp_feature_taxonomy_default_vocabularies() {
  return array(
    'edp_setting_keys' => array(
      'name' => 'edp | Setting Keys',
      'machine_name' => 'edp_setting_keys',
      'description' => 'Keys for the content type edp | Setting',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
