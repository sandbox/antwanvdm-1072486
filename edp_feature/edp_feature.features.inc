<?php
/**
 * @file
 * edp_feature.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function edp_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_views_api().
 */
function edp_feature_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => 3.0);
  }
}

/**
 * Implementation of hook_node_info().
 */
function edp_feature_node_info() {
  $items = array(
    'edp_domain' => array(
      'name' => t('edp | Domain'),
      'base' => 'node_content',
      'description' => t('A domain'),
      'has_title' => '1',
      'title_label' => t('Domain name'),
      'help' => '',
    ),
    'edp_setting' => array(
      'name' => t('edp | Setting'),
      'base' => 'node_content',
      'description' => t('A settings (key, value) belonging to a domain'),
      'has_title' => '1',
      'title_label' => t('Value'),
      'help' => '',
    ),
  );
  return $items;
}
