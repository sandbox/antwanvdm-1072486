/**
 * Javascript functionality for Facebook login popup, returns data from PHP to popup opener<br>
 * Always implement "handleSocialMediaData()" within your custom .JS file!
 */
jQuery(document).ready(function() {
  var fbdata = Drupal.settings.facebook.data;
  fbdata.source = "facebook";
  window.opener.handleSocialMediaData(fbdata);
  window.close();
});