<?php
/**
 * Class to wrap up most used Facebook features with the Facebook API
 * 
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */
class FacebookService {
  /**
   * Facebook Application ID
   * @var string
   */
  protected $appId;
  
  /**
   * Facebook Application Secret
   * @var string
   */
  protected $appSecret;
  
  /**
   * Facebook API Key
   * @var string
   */
  protected $apiKey;
  
  /**
   * Variable to be used as Facebook object
   * @var Facebook
   */
  public $facebookApi;
  
  /**
   * The SessionKey of the current session as Array
   * @var array
   */
  public $sessionArrayKey;
  
  /**
   * The SessionKey of the current session in JSON format
   * @var string
   */
  public $sessionJsonKey;
  
  /**
   * Constructor to init this object and create a Facebook object reference
   */
  public function __construct() {
    $this->appId = variable_get( "edp_facebook_app_id" );
    $this->appSecret = variable_get( "edp_facebook_app_secret" );
    $this->apiKey = variable_get( "edp_facebook_api_key" );
    
    $this->facebookApi = $this->initFacebookApi();
  }
  
  /**
   * Init new Facebook object
   *
   * @return Facebook
   */
  private function initFacebookApi() {
    return new Facebook( array(
      'appId' => $this->appId, 
      'secret' => $this->appSecret 
    ) );
  }
  
  /**
   * Validate is the session is valid or not
   * 
   * @return bool
   */
  public function validateSession() {
    $session = $this->facebookApi->getSession();
    
    if ($session) {
      return TRUE;
    }
    
    return FALSE;
  }
  
  /**
   * Return the userId of the loggedIn user, 0 if not available
   * 
   * @return int
   */
  public function getUserId() {
    return ( int ) $this->facebookApi->getUser();
  }
  
  /**
   * Return loggedIn user, combined with metadata if given argument is TRUE
   * @link http://developers.facebook.com/docs/reference/api/user/
   * 
   * @param bool $metadata
   * @return array
   */
  public function getUser($metadata = FALSE) {
    $metadataString = ($metadata == TRUE ? "?metadata=1" : "");
    return $this->apiCall( '/me' . $metadataString );
  }
  
  /**
   * Return feed from loggedIn user
   * @link http://developers.facebook.com/docs/reference/api/user/
   * 
   * @return array
   */
  public function getUserFeed() {
    return $this->apiCall( '/me/feed' );
  }
  
  /**
   * Return friends from loggedIn user
   * @link http://developers.facebook.com/docs/reference/api/user/
   * 
   * @return array
   */
  public function getFriends() {
    return $this->apiCall( '/me/friends' );
  }
  
  /**
   * Return likes from loggedIn user
   * @link http://developers.facebook.com/docs/reference/api/user/
   * 
   * @return array
   */
  public function getLikes() {
    return $this->apiCall( '/me/likes' );
  }
  
  /**
   * Do a WallPost on loggedIn users Wall with passed values
   * @link http://developers.facebook.com/docs/reference/api/post/
   * 
   * @param object $update->message      (Required) Message of the fb wallpost
   *               $update->title        (Required) The name of the box below the wallpost
   *               $update->link         (Required) The link shared within the wallpost name below the wallpost
   *               $update->caption      (Required) Subtitle behind given name below the wallpost
   *               $update->description  (Required) Text description below the wallpost
   *               $update->picture      (Required) Image share URL, also visible below the wallpost
   *               $update->source       (Optional) Link to an actual video source (.flv file) NOT a youtube/vimeo url link!!!
   *               $update->actionName   (Optional) Name of action next to like/comment status (example: watch on Vimeo)
   *               $update->actionLink   (Optional) Link behind $actionName next to like/comment status (example: http://www.vimeo.com/2342313)
   * @return bool
   */
  public function postStatusUpdate($update) {
    $wallpost = array(
      'message' => utf8_encode( $update->message ), 
      'name' => $update->title, 
      'link' => $update->link, 
      'caption' => $update->caption, 
      'description' => $update->description, 
      'picture' => $update->picture 
    );
    
    if (isset( $update->source )) {
      $wallpost ["source"] = $update->source;
    }
    
    if (isset( $update->actionName ) && isset( $update->actionLink )) {
      $wallpost ["actions"] = array(
        "name" => $update->actionName, 
        "link" => $update->actionLink 
      );
    }
    
    return $this->apiCall( '/me/feed', TRUE, $wallpost );
  }
  
  /**
   * Get the albums from the loggedIn user
   * @link http://developers.facebook.com/docs/reference/api/album/
   * 
   * @return array|string
   */
  public function getAlbums() {
    $albums = $this->apiCall( '/me/albums' );
    if (isset($albums["error"])) return $albums;
    
    //Check if data has any albums before continuing
    if (count($albums['data']) > 0) {
      $result = array();
      $i = 0;
      
      //Loop thrugh results and get photos by albums
      foreach ( $albums ['data'] as $album ) {
        $result [$i] ['name'] = $album ['name'];
        $result [$i] ['images'] = $this->getAlbumPhotos( $album ['id'] );
        $i ++;
      }
    }else{
        //Return custom array error
      $result = array(
        "error" => array(
          "type" => "CustomError", 
          "message" => "No Albums are found or permissions are not set" 
        ) 
      );
    }
    
    return $result;
  }
  
  /**
   * Get all the photos out a album from a user by albumId
   * @link http://developers.facebook.com/docs/reference/api/photo/
   * 
   * @param int $albumId
   * @return array
   */
  private function getAlbumPhotos($albumId) {
    $photos = $this->apiCall( "/$albumId/photos" );
    if (isset($photos["error"])) return $photos;
    
    $result = array();
    $i = 0;
    
    //Loop thrugh results and put photo's in array
    foreach ( $photos ['data'] as $photo ) {
      $result [$i] ['thumb'] = $photo ['picture'];
      $result [$i] ['large'] = $photo ['source'];
      $i ++;
    }
    
    return $result;
  }
  
  /**
   * Make an API call through the Facebook API class
   *
   * @param array $params   The API call parameters
   * @param bool  $post     Defines if API call is a POST or not
   * @param array $postdata If it's a POST, this contains the postdata
   * @return array
   */
  private function apiCall($call, $post = FALSE, $postdata = array()) {
    try {
      $data = ($post == FALSE ? $this->facebookApi->api( $call ) : $this->facebookApi->api( $call, 'POST', $postdata ));
      return $data;
    } catch ( FacebookApiException $e ) {
      return $e->getResult();
    }
  }
  
  /**
   * Set the Session and set the Array within a local Variable
   * 
   * @param string $sessionKey
   * @return array
   */
  public function setSession($sessionKey) {
    $this->sessionJsonKey = $sessionKey;
    $this->sessionArrayKey = json_decode( get_magic_quotes_gpc() ? stripslashes( $sessionKey ) : $sessionKey, TRUE );
    
    $this->facebookApi->setSession( $this->sessionArrayKey );
    return $this->sessionArrayKey;
  }
  
  /**
   * Return all possible permissions
   * @link http://developers.facebook.com/docs/authentication/permissions/
   * 
   * @return $permArray
   */
  public function getAllPerms() {
    $permArray = array(
      "publish_stream", 
      "create_event", 
      "rsvp_event", 
      "sms", 
      "offline_access", 
      "publish_checkins", 
      "user_about_me", 
      "user_activities", 
      "user_birthday", 
      "user_education_history", 
      "user_events", 
      "user_groups", 
      "user_hometown", 
      "user_interests", 
      "user_likes", 
      "user_location", 
      "user_notes", 
      "user_online_presence", 
      "user_photo_video_tags", 
      "user_photos", 
      "user_relationships", 
      "user_relationship_details", 
      "user_religion_politics", 
      "user_status", 
      "user_videos", 
      "user_website", 
      "user_work_history", 
      "friends_about_me", 
      "friends_activities", 
      "friends_birthday", 
      "friends_education_history", 
      "friends_events", 
      "friends_groups", 
      "friends_hometown", 
      "friends_interests", 
      "friends_likes", 
      "friends_location", 
      "friends_notes", 
      "friends_online_presence", 
      "friends_photo_video_tags", 
      "friends_photos", 
      "friends_relationships", 
      "friends_relationship_details", 
      "friends_religion_politics", 
      "friends_status", 
      "friends_videos", 
      "friends_website", 
      "friends_work_history", 
      "manage_friendlists", 
      "friends_checkins", 
      "email", 
      "read_friendlists", 
      "read_insights", 
      "read_mailbox", 
      "read_requests", 
      "read_stream", 
      "xmpp_login", 
      "ads_management", 
      "user_checkins", 
      "manage_pages" 
    );
    
    return $permArray;
  }
}
?>