/**
 * Constants for popup width/height (IE does not support the 'const' keyword)
 */
var FACEBOOK_POPUP_WIDTH = 700;
var FACEBOOK_POPUP_HEIGHT = 400;

/**
 * Function to be called for login popup of facebook
 * 
 * @return {Boolean}
 */
function loginPopupFacebook() {
  window.open("/facebook/login/popup", 'windowpopup', 'width=' + FACEBOOK_POPUP_WIDTH + ', height=' + FACEBOOK_POPUP_HEIGHT + ', toolbar=false');
  return false;
}