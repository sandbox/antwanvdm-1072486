<?php
/**
 * Class to Share your URL link to Social Media
 *
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */
class SocialMediaShareLink {
  /**
   * Initialise SocialMediaShareLink object
   *
   * @return  void
   */
  public function __construct() {
    //
  }
  
  /**
   * Returns the Hyves share URL
   *
   * @param   String   $name    Name for the share
   * @param   String   $message Given message as description of the name
   * @param   String   $type    Type of sharelink, default is 12 (Website)
   * @param   String   $rating  Rating of the sharelink, default is 5 (highest)
   * @return  String            Hyves Share URL
   */
  public function hyves($name, $message, $type = '12', $rating = '5') {
    $url = "http://www.hyves.nl/profielbeheer/toevoegen/tips/";
    $url .= "?name=" . $name;
    $url .= "&text=" . $message;
    $url .= "&type=" . $type;
    $url .= "&rating=" . $rating;
    return $url;
  }
  
  /**
   * Returns the Facebook share URL
   *
   * @param   String   $shareUrl URL to be shared
   * @param   String   $title    Title for the given URL
   * @return  String             Facebook Share URL
   */
  public function facebook($shareUrl, $title) {
    $url = "http://www.facebook.com/sharer.php";
    $url .= "?u=" . urlencode( $shareUrl );
    $url .= "&t=" . urlencode( $title );
    return $url;
  }
  
  /**
   * Returns the Twitter share URL
   *
   * @param   String   $text     Message to send to Twitter
   * @param   String   $shareUrl URL to be shared
   * @return  String             Twitter Share URL
   */
  public function twitter($text, $shareUrl) {
    $url = "http://twitter.com/share";
    $url .= "?text=" . urlencode( $text );
    $url .= "&url=" . urlencode( $shareUrl );
    return $url;
  }
  
  /**
   * Returns the LinkedIn share URL
   *
   * @param   String   $shareUrl URL to be shared
   * @param   String   $title    Title for the given URL
   * @param   String   $source   Source of the URL (example: Tweakers)
   * @param   String   $summary  Given summary as description of the title
   * @return  String             LinkedIn Share URL
   */
  public function linkedIn($shareUrl, $title, $source, $summary) {
    $url = "http://www.linkedin.com/shareArticle?mini=true";
    $url .= "&url=" . urlencode( $shareUrl );
    $url .= "&title=" . urlencode( $title );
    $url .= "&soure=" . urlencode( $source );
    $url .= "&summary=" . urlencode( $summary );
    return $url;
  }
  
  /**
   * Returns the MySpace share URL
   *
   * @param   String   $title    Title for the given URL
   * @param   String   $summary  Given summary as description of the title
   * @param   String   $shareUrl URL to be shared
   * @return  String             MySpace Share URL
   */
  public function mySpace($title, $summary, $shareUrl) {
    $url = "http://www.myspace.com/index.cfm?fuseaction=postto";
    $url .= "&t=" . urlencode( $title );
    $url .= "&c=" . urlencode( $summary );
    $url .= "&u=" . urlencode( $shareUrl );
    return $url;
  }
}
?>
