<?php
/**
 * Class to interact with more than one social media service
 * 
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */
class SocialInterface {
  /**
   * Facebook constant to use for active network
   * @var int
   */
  const SM_FACEBOOK = 1;
  
  /**
   * Twitter constant to use for active network
   * @var int
   */
  const SM_TWITTER = 2;
  
  /**
   * Hyves constant to use for active network
   * @var int
   */
  const SM_HYVES = 3;
  
  /**
   * Var to check which network is active, default to 0 (none active)
   * @var int
   */
  private $activeService = 0;
  
  /**
   * Initialise SocialInterface object
   */
  public function __construct() {
    //
  }
  
  /**
   * Set the current active
   * 1: Facebook 2: Twitter 3: Hyves
   * 
   * @param int $service Same numbers as const within this class
   */
  public function setActiveService($service) {
    $this->activeService = ( int ) $service;
  }
  
  /**
   * Get the active Service by Number
   * 1: Facebook 2: Twitter 3: Hyves 0: Unactive & no Session
   * 
   * @return int
   */
  public function getActiveService() {
    if ($this->activeService == 0) {
      $this->setActiveServiceByActiveSession();
    }
    
    return $this->activeService;
  }
  
  /**
   * Check for a service that is currently logged in with a Session
   * If available, set the active service
   */
  private function setActiveServiceByActiveSession() {
    if (isset( $_SESSION ["facebook_session_key"] )) {
      $this->setActiveService( self::SM_FACEBOOK );
    }
    if (isset( $_SESSION ["twitter_access_token"] )) {
      $this->setActiveService( self::SM_TWITTER );
    }
    if (isset( $_SESSION ["hyves_access_token"] )) {
      $this->setActiveService( self::SM_HYVES );
    }
  }
  
  /**
   * Get the current active Service. If more than one service is logged in,
   * you need to set $this->activeService before calling this function
   * 
   * @return FacebookService|TwitterService|HyvesService|SocialDefaultService
   */
  private function currentService() {
    //Set services to use if none are active yet
    if ($this->activeService == 0) {
      $this->setActiveServiceByActiveSession();
    }
    
    //Switch between services to see what needs to be returned
    switch ($this->activeService) {
      case self::SM_FACEBOOK :
        $service = $this->facebook();
        break;
      
      case self::SM_TWITTER :
        $service = $this->twitter();
        break;
      
      case self::SM_HYVES :
        $service = $this->hyves();
        break;
      
      default :
        $service = $this->defaultService();
    }
    
    return $service;
  }
  
  /**
   * Get user of the active network with details
   * 
   * @return array
   */
  public function getUser() {
    return $this->currentService()->getUser();
  }
  
  /**
   * Get friends of the active network
   * 
   * @return array
   */
  public function getFriends() {
    return $this->currentService()->getFriends();
  }
  
  /**
   * Post a status update to the active network
   * 
   * @param object $update
   * @return bool
   */
  public function postStatusUpdate($update) {
    return $this->currentService()->postStatusUpdate( $update );
  }
  
  /**
   * Directly use the Facebook Service
   * 
   * @return FacebookService
   */
  public function facebook() {
    $facebook = new FacebookService();
    $facebook->setSession( $_SESSION ["facebook_session_key"] );
    return $facebook;
  }
  
  /**
   * Directly use the Twitter Service
   * 
   * @return TwitterService
   */
  public function twitter() {
    $twitter = new TwitterService();
    $access_token = $_SESSION ["twitter_access_token"];
    $twitter->setNewOAuth( $access_token ['oauth_token'], $access_token ['oauth_token_secret'] );
    return $twitter;
  }
  
  /**
   * Directly use the Hyves Service
   * 
   * @return HyvesService
   */
  public function hyves() {
    $hyves = new HyvesService();
    $hyves->setAccessToken( $_SESSION ["hyves_access_token"] );
    return $hyves;
  }
  
  /**
   * If no service is active, let's use the default 'empty' service
   * 
   * @return SocialDefaultService
   */
  private function defaultService() {
    return new SocialDefaultService();
  }
}

/**
 * Basic 'service' class if no active service exists
 * 
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */
class SocialDefaultService {
  /**
   * Error string to return to application within every function
   * @var string
   */
  private $returnString = "No active service";
  
  /**
   * @see SocialInterface::getUser()
   */
  final public function getUser() {
    return $this->returnString;
  }
  
  /**
   * @see SocialInterface::getFriends()
   */
  final public function getFriends() {
    return $this->returnString;
  }
  
  /**
   * @see SocialInterface::postStatusUpdate();
   */
  final public function postStatusUpdate() {
    return $this->returnString;
  }
}