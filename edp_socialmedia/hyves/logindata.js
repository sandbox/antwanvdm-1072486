/**
 * Javascript functionality for Hyves login popup, returns data from PHP to popup opener<br>
 * Always implement "handleSocialMediaData()" within your custom .JS file!
 */
jQuery(document).ready(function() {
  var hydata = Drupal.settings.hyves.data;
  hydata.source = "hyves";
  window.opener.handleSocialMediaData(hydata);
  window.close();
});