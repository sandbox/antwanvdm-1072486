/**
 * Constants for popup width/height (IE does not support the 'const' keyword)
 */
var HYVES_POPUP_WIDTH = 800;
var HYVES_POPUP_HEIGHT = 400;

/**
 * Function to be called for login popup of hyves
 * 
 * @return {Boolean}
 */
function loginPopupHyves() {
  window.open("/hyves/login/popup", 'windowpopup', 'width=' + HYVES_POPUP_WIDTH + ', height=' + HYVES_POPUP_HEIGHT + ', toolbar=false');
  return false;
}