<?php
/**
 * Class to wrap up most used Hyves features with the Hyves API
 * 
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */
class HyvesService {
  /**
   * Hyves consumer key
   * @var string
   */
  protected $consumerKey;
  
  /**
   * Hyves consumer secret
   * @var string
   */
  protected $consumerSecret;
  
  /**
   * Hyves API version
   * @var string
   */
  protected $apiVersion;
  
  /**
   * Variable to be used as Hyves object
   * @var GenusApis
   */
  public $hyvesApi;
  
  /**
   * Variable that contains a serialized Hyves access token
   * @var string
   */
  private $accesstoken;
  
  /**
   * Constructor to init this object and create a Hyves object reference
   */
  public function __construct() {
    $this->consumerKey = variable_get( "edp_hyves_consumer_key" );
    $this->consumerSecret = variable_get( "edp_hyves_consumer_secret" );
    $this->apiVersion = variable_get( "edp_hyves_api_version" );
    
    $this->hyvesApi = $this->initHyvesApi();
  }
  
  /**
   * Init new Hyves object
   *
   * @return GenusApis
   */
  private function initHyvesApi() {
    try {
      $oOAuthConsumer = new OAuthConsumer( $this->consumerKey, $this->consumerSecret );
      $api = new GenusApis( $oOAuthConsumer, $this->apiVersion );
    } catch ( GeneralException $APIError ) {
      return FALSE;
    }
    return $api;
  }
  
  /**
   * Get the current user
   * @link http://www.hyves-developers.nl/documentation/data-api/methods/2.0/users.getloggedin
   * 
   * @param array $responseFields Fields that are extra returned by API call; array("profilepicture","cityname")
   * @return array
   */
  public function getUser($responseFields = array()) {
    return $this->apiCall( "users.getLoggedin", array(
      "ha_responsefields" => implode(",", $responseFields) 
    ) );
  }
  
  /**
   * Get all the friends of the loggedIn user
   * @link http://www.hyves-developers.nl/documentation/data-api/methods/2.0/users.getfriendsbyloggedinsorted
   * 
   * @param string $sorttype       The way to order the friends
   * @param array $responseFields  Fields that are extra returned by API call; array("profilepicture","aboutme")
   * @return array
   */
  public function getFriends($sorttype = 'alphabetically', $responseFields = array()) {
    //Initiate the result array
    $result = array();
    
    //Set the number of pages to 1, after the first call, we will change this
    $pages = 1;
    
    //Get all friends
    for ($i = 1; $i <= $pages; $i ++) {
      //Do the API call for each page. The call returns 100 friends per page.
      //Friends are sorted alphabetically by firstname	
      $friends = $this->apiCall( "users.getFriendsByLoggedinSorted", array(
        'sorttype' => $sorttype, 
        'ha_responsefields' => implode(",", $responseFields), 
        'ha_page' => $i, 
        'ha_resultsperpage' => 100 
      ) );
      
      //Error means, breaking & returning original data
      if (isset($friends->error_code)) {
        $result = $friends;
        break;
      }
      
      //Create a record for each friend and place it in the result-array.
      foreach ( $friends as $friend ) {
        if ($friend->userid != ''){
          $result [] = $friend;
        }
        
        //Set total pages so the loop will continue
        $pages = $friends->totalpages;
      }
    }
    
    return $result;
  }
  
  /**
   * Get all Media (photos) from current loggedIn user
   * @link http://www.hyves-developers.nl/documentation/data-api/methods/2.0/media.getbyloggedin
   * 
   * @param array $responseFields Fields that are extra returned by API call; array("respectscount","tags")
   * @return array
   */
  public function getMedia($responseFields = array()) {
    //Initiate the result array
    $result = array();
    
    //Set the number of pages to 1, after the first call, we will change this
    $pages = 1;
    
    //Get all media
    for ($i = 1; $i <= $pages; $i ++) {
      //Do the API call for each page. The call returns 100 media-items per page.
      //Media is sorted by age, newest first
      $media = $this->apiCall( "media.getByLoggedin", array(
      	'ha_responsefields' => implode(",", $responseFields), 
      	'ha_page' => $i, 
        'ha_resultsperpage' => "100" 
      ) );
      
      //Error means, breaking & returning original data
      if (isset($media->error_code)) {
        $result = $media;
        break;
      }
      
      //Loop through the items and add in Array
      foreach ( $media as $item ) {
        if ($item->title != "") {
          $result [] = $item;
        }
        
        //Set total pages so the loop will continue
        $pages = $item->totalpages;
      }
    }
    
    return $result;
  }
  
  /**
   * Post a "Wie Wat Waar" on Hyves
   * @link http://www.hyves-developers.nl/documentation/data-api/methods/2.0/wwws.create
   * 
   * @param object $update->what        (Required) The update message itself
   *               $update->visibility  (Required) The modus of visibility (who can see it)
   *               $update->where       (Optional) The place @ where you are or link to
   *               $update->hubId       (Optional) Choose a hubId of the User instead of 'where'
   *               $update->spotId      (Optional) Choose a spotId of the User instead of 'where'
   *               $update->lat         (Optional) Current latitude
   *               $update->long        (Optional) Current longitude
   * @param array  $responseFields      Fields that are extra returned by API call; array("geolocation","respectscount")
   * @return array
   */
  public function postStatusUpdate($update, $responseFields = array()) {
    $parameters = array(
      'ha_version' => $this->apiVersion, 
      'emotion' => $update->what, 
      'visibility' => $update->visibility, 
      'ha_responsefields' => implode(",", $responseFields)
    );
    
    if (isset( $update->where ))
      $parameters ["where"] = $update->where;
    if (isset( $update->hubId ))
      $parameters ["hubid"] = $update->hubId;
    if (isset( $update->spotId ))
      $parameters ["privatespotid"] = $update->spotId;
    if (isset( $update->lat ))
      $parameters ["latitude"] = $update->lat;
    if (isset( $update->long ))
      $parameters ["longitude"] = $update->long;
    
    return $this->apiCall( "wwws.create", $parameters );
  }
  
  /**
   * Post a "Tip" on Hyves
   * @link http://www.hyves-developers.nl/documentation/data-api/methods/2.0/tips.create
   * 
   * @param string $title          The title of the Tip itself
   * @param string $message        The message containing a description of the Tip
   * @param int $categoryId        A categoryId of the category te Tip belongs with, default to 'Website'
   * @param int $rating            A rating number (1-5) for the Tip
   * @param array $responseFields  Fields that are extra returned by API call; array("respectscount","viewscount")
   * @return array
   */
  public function createTip($title, $message, $categoryId = '00be7fa4a7eb16282922bb1236a139cfe3', $rating = 5, $responseFields = array()) {
    return $this->apiCall( "tips.create", array(
      'ha_version' => $this->apiVersion, 
      'title' => $title, 
      'body' => $message, 
      'tipcategoryid' => $categoryId, 
      'rating' => $rating, 
      'ha_responsefields' => implode( ",", $responseFields ) 
    ) );
  }
  
  /**
   * Place the gadget on the logged-in users hyves
   * @link http://www.hyves-developers.nl/documentation/data-api/methods/2.0/gadgets.createbyxml
   * 
   * @param string $gadgetUrl   URL of the XML file for the Gadget
   * @param string $visibility  The modus of visibility (who can see it), can be 'public'
   * @param string $profilePage Show gadget on your profile page; 'true' or 'false'
   * @param string $homePage    Show gadget on the homepage; 'true' or 'false'
   * @return array
   */
  public function createGadgetXML($gadgetUrl, $visibility, $profilePage = 'true', $homePage = 'false') {
    return $this->apiCall( "gadgets.createByXML", array(
      'ha_version' => $this->apiVersion, 
      'specurl' => $gadgetUrl, 
      'visibility' => $visibility, 
      'onprofilepage' => $profilePage, 
      'onhomepage' => $homePage 
    ) );
  }
  
  /**
   * Convert a openSocialId to a valid HyvesId
   * 
   * @param string $openSocialId
   * @return int
   */
  public function getHyvesIdByOpenSocialId($openSocialId) {
    $user = $this->apiCall( "users.get", array(
      'userid' => $openSocialId 
    ) );
    $hyvesId = ( string ) $user->user->userid;
    return $hyvesId;
  }
  
  /**
   * Do the API call to the server with the given method & params
   * @todo Try to fix the 'simpleXmlToArray' functionality (works, but not with current functions)
   * 
   * @param string $method
   * @param array $params
   * @return array|string
   */
  private function apiCall($method, $params) {
    try {
      $result = $this->hyvesApi->doMethod( $method, $params, $this->getAccessToken() );
//      if ($result instanceof SimpleXMLElement) {
//        $result = $this->simpleXmlToArray($result);
//      }
    } catch ( HyvesApiException $e ) {
      return $e->getResponse();
    }
    
    return $result;
  }
  
  /**
   * Convert SimpleXMLElement Object to Array
   * 
   * @param SimpleXMLElement $xmlobj
   * @return array
   */
  private function simpleXmlToArray(SimpleXMLElement $xmlobj) {
    $a = array();
    //Loop through all XML children
    foreach ( $xmlobj->children() as $node ) {
      //If item is still XML, parse content through function again
      if ($node instanceof SimpleXMLElement && count( $node ) > 1) {
        $a [$node->getName()] = $this->simpleXmlToArray( $node );
      } else {
        //If item allready exists, its an inside array; avoid override!
        if (isset( $a [$node->getName()] )) {
          //First time in, get the first string, create array and add first value
          if (is_string( $a [$node->getName()] )) {
            $oldname = $a [$node->getName()];
            $a [$node->getName()] = array();
            $a [$node->getName()] [] = $oldname;
          }
          $a [$node->getName()] [] = ( string ) $node [0];
        } else {
          //If it doesn't exist, just put in the string as valid value
          $a [$node->getName()] = ( string ) $node [0];
        }
      }
    }
    return $a;
  }
  
  /**
   * Set the accesstoken so it can be used for later use
   * 
   * @param array $access_token
   */
  public function setAccessToken($access_token) {
    $this->accesstoken = serialize( $access_token );
  }
  
  /**
   * Get the accesstoken that was stored earlier
   * 
   * @return array
   */
  private function getAccessToken() {
    return unserialize( $this->accesstoken );
  }
  
  /**
   * Set the requestToken within a session
   * 
   * @param array $request_token
   */
  public function setRequestTokenSession($request_token) {
    $_SESSION ['requesttoken_' . $request_token->getKey()] = serialize( $request_token );
  }
  
  /**
   * Get the requestToken out of an active session, if no session return FALSE
   * 
   * @param string $oauth_token
   * @return array|bool
   */
  public function getRequestTokenFromSession($oauth_token) {
    if (! isset( $_SESSION ['requesttoken_' . $oauth_token] )) {
      return FALSE;
    }
    return unserialize( $_SESSION ['requesttoken_' . $oauth_token] );
  }
}