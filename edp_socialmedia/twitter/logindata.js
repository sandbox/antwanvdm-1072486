/**
 * Javascript functionality for Twitter login popup, returns data from PHP to popup opener<br>
 * Always implement "handleSocialMediaData()" within your custom .JS file!
 */
jQuery(document).ready(function() {
  var twdata = Drupal.settings.twitter.data;
  twdata.source = "twitter";
  window.opener.handleSocialMediaData(twdata);
  window.close();
});