<?php
/**
 * Class to wrap up most used Twitter features with the Twitter API
 * 
 * @author Antwan van der Mooren <antwan.van.der.mooren@dpdk.nl>
 * @package Extended Drupal Package
 * @version 7.x-1.0-dev
 */
class TwitterService {
  /**
   * Twitter consumer key
   * @var string
   */
  protected $consumerKey;
  
  /**
   * Twitter consumer secret
   * @var string
   */
  protected $consumerSecret;
  
  /**
   * Twitter oAuth callback
   * @var string
   */
  protected $oAuthCallback;
  
  /**
   * Variable to be used as Twitter object
   * @var TwitterOAuth
   */
  public $twitterApi;
  
  /**
   * The AccessToken of the current session
   * @var array
   */
  public $accessToken;
  
  /**
   * Return Array to log and show us what happened
   * @var array
   */
  public $returnData = array();
  
  /**
   * Constructor to init this object and create a TwitterOAuth instance
   */
  public function __construct() {
    $this->consumerKey = variable_get( "edp_twitter_consumer_key" );
    $this->consumerSecret = variable_get( "edp_twitter_consumer_secret" );
    $this->oAuthCallback = variable_get( "edp_twitter_oauth_callback" );
    
    $this->twitterApi = $this->initTwitterApi();
  }
  
  /**
   * Init new Twitter object
   *
   * @return TwitterOAuth
   */
  private function initTwitterApi() {
    $token = (isset( $_SESSION ['oauth_token'] ) ? $_SESSION ['oauth_token'] : NULL);
    $secret = (isset( $_SESSION ['oauth_token_secret'] ) ? $_SESSION ['oauth_token_secret'] : NULL);
    
    return new TwitterOAuth( $this->consumerKey, $this->consumerSecret, $token, $secret );
  }
  
  /**
   * Get a requesttoken (Wrapper function for the API)
   * 
   * @return array
   */
  public function getRequestToken() {
    return $this->twitterApi->getRequestToken( $this->oAuthCallback );
  }
  
  /**
   * Get the current loggedIn user
   * @link http://dev.twitter.com/doc/get/account/verify_credentials
   * 
   * @param bool Include entities for each tweet, default to FALSE
   * @return array
   */
  public function getUser($entities = FALSE) {
    return $this->apiCall( 'get', 'account/verify_credentials', array(
      'include_entities' => $entities 
    ) );
  }
  
  /**
   * Get the current loggedId users timeline
   * @link http://dev.twitter.com/doc/get/statuses/user_timeline
   * 
   * @param array $parameters Optional parameters that are possible to use in this context, default to none
   * @return array
   */
  public function getUserTimeline($parameters = array()) {
    return $this->apiCall( 'get', 'statuses/user_timeline', $parameters );
  }
  
  /**
   * Get all the friend ID's of the loggedIn user
   * @link http://dev.twitter.com/doc/get/friends/ids
   * 
   * @return array
   */
  public function getFriends() {
    return $this->apiCall( 'get', 'friends/ids' );
  }
  
  /**
   * Place a status update on your Twitter account
   * @link http://dev.twitter.com/doc/post/statuses/update
   * 
   * @param object $update->message  (Required) Message to be posted as tweet
   *               $update->replyId  (Optional) ID of tweet to reply to
   *               $update->lat      (Optional) Current latitude (only works if user accepts location tracking)
   *               $update->long     (Optional) Current longitude (only works if user accepts location tracking)
   *               $update->placeId  (Optional) ID of any place, retrieved from geo/reverse_geocode
   *               $update->coords   (Optional) Display coordinates, set to TRUE if lat/lon are set
   * @param bool   $trim_user        Include entities for each tweet, default to FALSE
   * @param bool   $entities         Include entities for each tweet, default to FALSE
   * @return array
   */
  public function postStatusUpdate($update, $trim_user = FALSE, $entities = FALSE) {
    $parameters = array(
      'status' => $update->message, 
      'trim_user' => $trim_user, 
      'include_entities' => $entities 
    );
    
    if (isset( $update->replyId ))
      $parameters ["in_reply_to_status_id"] = $update->replyId;
    if (isset( $update->lat ))
      $parameters ["lat"] = $update->lat;
    if (isset( $update->long ))
      $parameters ["long"] = $update->long;
    if (isset( $update->placeId ))
      $parameters ["place_id"] = $update->placeId;
    if (isset( $update->coords ))
      $parameters ["display_coordinates"] = $update->coords;
    
    return $this->apiCall( 'post', 'statuses/update', $parameters );
  }
  
  /**
   * Do the API call to the server with the given method & params
   * 
   * @param string $action     Action do be done, either 'post' or 'get'
   * @param string $method     The method to be exuctued through the API
   * @param array $parameters  Optional parameters to be send with method
   * @return array
   */
  private function apiCall($action, $method, $parameters = array()) {
    if ($action == 'get' || $action == 'post') {
      $status = $this->twitterApi->$action( $method, $parameters );
      $result = $status;
    } else {
      //If action is invalid, return error
      $result = array(
        "error" => array(
          "type" => "CustomError", 
          "message" => "Provide valid \$action (get or post) in apiCall()" 
        ) 
      );
    }
    
    return $result;
  }
  
  /**
   * Set a new oAuth client for a new session (different per user)
   * 
   * @param string $oauthToken
   * @param string $oauthSecret
   */
  public function setNewOAuth($oauthToken, $oauthSecret) {
    $this->twitterApi = new TwitterOAuth( $this->consumerKey, $this->consumerSecret, $oauthToken, $oauthSecret );
  }
  
  /**
   * Set the AccessToken within a local Variable
   * 
   * @param array $accessToken
   * @return array
   */
  public function setAccessToken($accessToken) {
    $this->accessToken = $accessToken;
    return $this->accessToken;
  }
}