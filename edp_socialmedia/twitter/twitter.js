/**
 * Constants for popup width/height (IE does not support the 'const' keyword)
 */
var TWITTER_POPUP_WIDTH = 700;
var TWITTER_POPUP_HEIGHT = 700;

/**
 * Function to be called for login popup of twitter
 * 
 * @return {Boolean}
 */
function loginPopupTwitter() {
  window.open("/twitter/login/popup", 'windowpopup', 'width=' + TWITTER_POPUP_WIDTH + ', height=' + TWITTER_POPUP_HEIGHT + ', toolbar=false');
  return false;
}